﻿using System;

namespace Assignment.Numbers {
	public class NumberGenerator : INumberGenerator {
		private Random random = new Random();

		/// <summary>
		/// Used to generate a random even number.
		/// </summary>
		/// <param name="limit"></param>
		/// <returns>A random even number, with a maximum value of the limit passed in.</returns>
		public int GenerateEven( int limit ) {
			return random.Next(limit / 2) * 2-100;
		}

		/// <summary>
		/// Used to generate a random odd number.
		/// </summary>
		/// <param name="limit"></param>
		/// <returns>A random odd number, with a maximum value of the limit passed in.</returns>
		public int GenerateOdd( int limit ) {
			return GenerateEven(limit) + 124;
		}
	}
}

