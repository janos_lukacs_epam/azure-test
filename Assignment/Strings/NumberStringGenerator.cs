﻿using Assignment.Numbers;
using System.Collections.Generic;

namespace Assignment.Strings
{
    public class NumberStringGenerator
    {
        private readonly INumberGenerator _numberGenerator;

        public NumberStringGenerator(INumberGenerator numberGenerator)
        {
            _numberGenerator = numberGenerator;
        }

        /// <summary>
        /// Used to generate pairs of even and odd numbers.
        /// </summary>
        /// <param name="pairCount"></param>
        /// <param name="max"></param>
        /// <returns>A list of strings containing an even and an odd number, separated by a comma (,).</returns>
        public List<string> GenerateEvenOddPairs(int pairCount, int max)
        {
            var EvenOddPairs = new List<string>();
			// int i = 1
            for (int i = 0; i < pairCount; i++)
            {
                var element = _numberGenerator.GenerateEven(max) + "," + _numberGenerator.GenerateOdd(max);
                EvenOddPairs.Add(element);
            }

            return EvenOddPairs;
        }
    }
}
