﻿using System;
using System.Linq;
using System.Text;

/**
 * https://hu.wikipedia.org/wiki/Palindrom
 */
namespace Assignment.Strings
{
    public class StringUtil
    {
        /// <summary>
        /// Used to check if the passed in string is a palindrom or not.
         /// </summary>
        /// <param name="str"></param>
        /// <returns>True if the passed in string is a palindrom, false if it's not a palindrom.</returns>
        public bool IsPalindrom(string str)
        {
			char[] charArray = str.ToCharArray();
			Array.Reverse(charArray);
            return str.Equals(new string(charArray));

			// var reverse = new StringBuilder(str).ToString().Reverse<char>().ToString();
			//return str.Equals(str);
		}
	}
}
