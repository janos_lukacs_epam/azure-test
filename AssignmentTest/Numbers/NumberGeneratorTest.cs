﻿using Assignment.Numbers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentTest {
	[TestFixture]
	public class NumberGeneratorTest {
		NumberGenerator ng;

		[OneTimeSetUp]
		public void Init() {
			ng = new NumberGenerator();
		}

		[Test]
		public void GenerateEven_Should_GenerateEvenNumbers() {
			List<int> n = new List<int>();
			for ( int i = 0; i < 100; i++ )
				n.Add(ng.GenerateEven(10));

			Assert.That(!n.Where(x => x % 2 == 1).Any());
		}

		[Test]
		public void GenerateOdd_Should_GenerateOddNumbers() {
			List<int> n = new List<int>();
			for ( int i = 0; i < 100; i++ )
				n.Add(ng.GenerateOdd(10));

			Assert.That(!n.Where(x => x % 2 == 0).Any(), "something's wrong dude");
		}

		[TestCase(3)]
		[TestCase(1000)]
		[TestCase(1)]
		[TestCase(64)]
		public void GenerateEven_Should_GenerateUnderLimit_When_GivenLimitAsParameter( int limit ) {
			List<int> n = new List<int>();
			for ( int i = 0; i < 100; i++ )
				n.Add(ng.GenerateEven(limit));

			Assert.That(!n.Any(x => x > limit));
		}

		[TestCase(3)]
		[TestCase(1000)]
		[TestCase(1)]
		[TestCase(64)]
		public void GenerateOdd_Should_GenerateUnderLimit_When_GivenLimitAsParameter( int limit ) {
			List<int> n = new List<int>();
			for ( int i = 0; i < 100; i++ )
				n.Add(ng.GenerateOdd(limit));

			Assert.That(!n.Any(x => x > limit));
		}
	}
}