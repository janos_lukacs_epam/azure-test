﻿using Assignment.Numbers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentTest {
	[TestFixture]
	class NumberUtilsTest {

		NumberUtils nu;

		[OneTimeSetUp]
		public void Init() {
			nu = new NumberUtils();
		}

		[TestCase(16, new int[] { 1, 2, 4, 8, 16 })]
		[TestCase(8, new int[] { 1, 2, 4, 8 })]
		[TestCase(4, new int[] { 1, 2, 4 })]
		public void GetDivisors_Should_ReturnDivisors_When_CalledWithValidParameters( int n, int[] divisors ) {
			var d = nu.GetDivisors(n);
			int i = 0;

			while ( i < divisors.Length && d.Where(x => x == divisors[i]).Any() )
				i++;

			Assert.That(i == divisors.Length);
		}

		[TestCase(7)]
		[TestCase(37)]
		[TestCase(11)]
		public void IsPrime_Should_ReturnTrue_When_CalledWithPrimeNumbers( int n ) {

			Assert.That(nu.IsPrime(n));
		}

		[TestCase(8)]
		[TestCase(1)]
		[TestCase(10)]
		public void IsPrime_Should_ReturnFalse_When_CalledWithPrimeNumbers( int n ) {

			Assert.That(!nu.IsPrime(n));
		}

		[TestCase(4)]
		[TestCase(1)]
		[TestCase(9)]
		[TestCase(34)]
		public void EvenOrOdd_Should_ReturnCorrectString_When_CalledWithValidParameters( int n ) {
			bool even = n % 2 == 0;
			string r = nu.EvenOrOdd(n);
			if ( even )
				Assert.That(r == "even");
			else
				Assert.That(r == "odd");
		}
	}
}
