﻿using Assignment.Numbers;
using Assignment.Strings;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentTest
{
	[TestFixture]
	class NumberStringGeneratorTest
	{
		NumberStringGenerator nsg;
		Mock<INumberGenerator> mock;

		[OneTimeSetUp]
		public void Init()
		{
			mock = new Mock<INumberGenerator>();

			mock.Setup(m => m.GenerateEven(20)).Returns(4);
			mock.Setup(m => m.GenerateOdd(20)).Returns(19);

			mock.Setup(m => m.GenerateEven(30)).Returns(26);
			mock.Setup(m => m.GenerateOdd(30)).Returns(15);

			nsg = new NumberStringGenerator(mock.Object);
		}

		[TestCase(1)]
		[TestCase(5)]
		[TestCase(8)]
		public void GenerateEvenOddPairs_Should_GenerateGivenAmmountOfItems_When_CalledWithValidParameter( int length )
		{
			var items = nsg.GenerateEvenOddPairs(length, 20);

			Assert.That(items.Count == length);
		}

		[TestCase(20, "4,19")]
		[TestCase(30, "26,15")]
		public void GenerateEvenOddPairs_Should_GenerateListContainingResult_When_CalledWithValidParameter( int max, string result )
		{
			var items = nsg.GenerateEvenOddPairs(5, max);

			if ( items.Count != 5 )
				throw new Exception("Bad count generated");

			Assert.That(items.Contains(result));
		}

		[TestCase(7)]
		[TestCase(2)]
		public void GenerateEvenOddPairs_Should_CallGenerateEvenExactTimes_When_CalledWithValidParameter( int pairCount )
		{
			Init();

			nsg.GenerateEvenOddPairs(pairCount, 10);

			mock.Verify(m => m.GenerateEven(10), Times.Exactly(pairCount));
		}

		[TestCase(8)]
		[TestCase(6)]
		public void GenerateEvenOddPairs_Should_CallGenerateOddExactTimes_When_CalledWithValidParameter( int pairCount )
		{
			Init();

			nsg.GenerateEvenOddPairs(pairCount, 10);

			mock.Verify(m => m.GenerateOdd(10), Times.Exactly(pairCount));
		}
	}
}
