﻿using Assignment.Strings;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentTest {
	[TestFixture]
	class StringUtilTest {
		StringUtil su;

		[OneTimeSetUp]
		public void Init() {
			su = new StringUtil();
		}

		[TestCase("asddsa")]
		[TestCase("asdasddsadsa")]
		[TestCase("zolioiloz")]
		public void IsPalindrom_Should_ReturnTrue_When_ParameterIsPalindrom( string p ) {
			bool isPalindrom = su.IsPalindrom(p);

			Assert.That(isPalindrom);
		}

		[TestCase("not a palindrom")]
		public void IsPalindrom_Should_ReturnFalse_When_ParameterIsNotPalindrom( string p ) {
			bool isPalindrom = su.IsPalindrom(p);

			Assert.That(!isPalindrom);
		}
	}
}
